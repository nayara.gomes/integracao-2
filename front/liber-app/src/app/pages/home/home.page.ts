
import { Component, OnInit } from '@angular/core';
import { BookServiceService } from '../../services/book/book-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  slideOpts = {
    slidesPerView : 3,
    slidesPerGroup: 3
  }
  wellRatedBooks: [];
  mostSoldBooks: [];
  books: [];


  constructor(public bookService: BookServiceService) { }

  ngOnInit() {
    this.getWellRatedBooks();
    this.getMostSoldBooks();
    this.listBooks();
  }

  getId(id){
    localStorage.setItem('book_id', id);
  }

  listBooks(){
    this.bookService.listBooks().subscribe(
      (res) => {
      this.books = res.books;
      console.log(this.books);
    },
    (err) => {
      console.log(err);
    })
  }

  getWellRatedBooks(){
    this.bookService.listMostWellRatedBooks().subscribe((res) => {
      this.wellRatedBooks = res;
      console.log(this.wellRatedBooks);
    });
  }

  getMostSoldBooks(){
    this.bookService.listMostSoldBooks().subscribe((res) =>{
      this.mostSoldBooks = res;
      console.log(this.mostSoldBooks);
    })
  }

}
