import { Component, OnInit } from '@angular/core';
import { CommentService } from 'src/app/services/comment.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookServiceService } from 'src/app/services/book/book-service.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.page.html',
  styleUrls: ['./book.page.scss'],
})
export class BookPage implements OnInit {

  commentForm: FormGroup;
  editCommentForm: FormGroup;
  book;
  comments;
  editMode = false;
  book_id = localStorage.getItem('book_id');
  comment_id: number;
  comment_text: any;

  constructor(public commentService: CommentService,
              public bookService: BookServiceService,
              public formbuilder: FormBuilder,) {
    this.commentForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
    })
    this.editCommentForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
    })
  }

  ngOnInit() {
    this.showBook();
  }

  showBook(){
    this.bookService.showBook(this.book_id).subscribe(
      (res) => {
        this.book = res.book;
        console.log(this.book);
      },
      (err) => {
        console.log(err);
      }
    )
  }

  sendComment() {
    
  }

  toggleEdit(id){
      this.comment_id = id;
      this.editMode=true; 
    }

  updateComment(){

  }


  

 

}
