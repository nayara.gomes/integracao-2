import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  apiUrl:string = 'http://localhost:8000/api/';

  constructor(public http: HttpClient) { }

  httpHeaders: any = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  }

  createBook(form): Observable<any>{
    return this.http.post(this.apiUrl + 'books', form);
  }

  showBook(id): Observable<any>{
    return this.http.get(this.apiUrl + 'books/' + id, this.httpHeaders);
  }

  listBooks(): Observable<any>{
    return this.http.get(this.apiUrl + 'books', this.httpHeaders);
  }

  updateBook(id): Observable<any>{
    return this.http.put(this.apiUrl + 'books/' + id, this.httpHeaders);
  }

  deleteBook(): Observable<any>{
    return this.http.delete(this.apiUrl + 'books', this.httpHeaders);
  }

  listMostSoldBooks(): Observable<any>{
    return this.http.get(this.apiUrl + 'mostSoldBooks', this.httpHeaders);
  }

  listMostWellRatedBooks(): Observable<any>{
    return this.http.get(this.apiUrl + 'mostWellRatedBooks', this.httpHeaders);
  }


}
